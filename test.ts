/// <reference types="mocha"/>
/// <reference types="node" />
import backoff from './index';
import * as assert from 'assert';

describe('backoff', () => {
  it('should create a stream', function(done: any) {
    this.timeout(20e3);

    const stream = backoff(100, 2, 2000).take(7);
    let expected = [0, 1, 2, 3, 4, 5, 6];
    stream.addListener({
      next: (x: number) => {
        console.log(x);
        const e = expected.shift();
        if (typeof e === 'number') {
          assert.equal(x, e);
        } else {
          assert.fail(undefined, e, 'e should be defined', '=');
        }
      },
      error: done,
      complete: () => {
        assert.equal(expected.length, 0);
        done();
      },
    });
  });
});
