import {InternalListener, InternalProducer, Stream} from 'xstream';

class Backoff implements InternalProducer<number> {
  public type = 'backoff';
  private period: number;
  private readonly factor: number;
  private readonly maximum: number;
  private intervalID: any;
  private i: number;

  constructor(period: number, factor: number, maximum: number) {
    this.period = period;
    this.factor = factor;
    this.maximum = maximum;
    this.intervalID = -1;
    this.i = 0;
  }

  _start(out: InternalListener<number>): void {
    const intervalHandler = () => {
      out._n(this.i++);
      clearInterval(this.intervalID);
      this.period *= this.factor;
      this.period = Math.min(this.period, this.maximum);
      this.intervalID = setInterval(intervalHandler, this.period);
    };
    this.intervalID = setInterval(intervalHandler, this.period);
  }

  _stop(): void {
    if (this.intervalID !== -1) clearInterval(this.intervalID);
    this.intervalID = -1;
    this.i = 0;
  }
}

/**
 * Creates an xstream Stream that emits numbers in exponential backoff periods of time.
 *
 * @param interval initial interval of time expressed in milliseconds
 * @param factor (optional, default `2) how much to multiply the previous `interval` each time the stream emits
 * @param maximum (optional, default Infinity) the maximum interval at which to limit the interval calculation
 */
export default function backoff(
  interval: number,
  factor: number = 2,
  maximum: number = Infinity,
): Stream<number> {
  return new Stream<number>(new Backoff(interval, factor, maximum));
}
