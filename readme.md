# `xstream-backoff`

```
pnpm install --save xstream-backoff
```

Creates an xstream Stream that emits numbers in exponential backoff periods of time.

## Description

Creates a stream that periodically emits incremental numbers, where the interval of time varies exponentially.

Marble diagram:

```text
           backoff(500, 2, 6000)
-0---1-------2---------------3-----------------------4----...

intervals:
500ms
    1s
            2s
                            4s
                                                    6s
```

Arguments:

- `interval`: initial interval of time expressed in milliseconds
- `factor`: (optional, default `2`) how much to multiply the previous `interval` each time the stream emits
- `maximum`: (optional, default `Infinity`) the maximum interval at which to limit the interval calculation

## Usage

```js
import backoff from 'xstream-backoff'

const source = backoff(500, 2, 6000).take(5)

source.addListener({
  next: i => console.log(i),
  error: err => console.error(err),
  complete: () => console.log('completed')
})
```

```text
> 0
> 1
> 2
> 3
> 4
```

## License

MIT
